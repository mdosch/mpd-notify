CC = clang
CFLAGS  = -Wall -lmpdclient
PREFIX = /usr/local/bin

TARGET = mpd-notify

all: $(TARGET)

$(TARGET): $(TARGET).c
	$(CC) $(CFLAGS) -o $(TARGET) $(TARGET).c

install: $(TARGET)
	cp $(TARGET) $(PREFIX)

uninstall: $(TARGET)
	rm -f $(PREFIX)/$(TARGET)
clean:
	$(RM) $(TARGET)
